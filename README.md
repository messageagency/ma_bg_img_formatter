# README #

This module provides a field formatter for image fields that renders the field as markup with the image as a background image style.

It does not provide any way for the background image to be set on the field's parent entity's wrapper, which is a common need (think billboards). That kind of functionality could be handled in ma_layouts module by adding config settings to a custom Layout class (tbd).
