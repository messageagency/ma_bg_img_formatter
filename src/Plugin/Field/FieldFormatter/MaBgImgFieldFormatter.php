<?php

namespace Drupal\ma_bg_img_formatter\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'MaBgImgFieldFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "ma_bg_img_field_formatter",
 *   label = @Translation("Background image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class MaBgImgFieldFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    unset($element['image_link']);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    return [$summary[0]];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    if (empty($images = $this->getEntitiesToView($items, $langcode))) {
      // Early opt-out if the field is empty.
      return $elements;
    }

    /** @var \Drupal\image\ImageStyleInterface $image_style */
    $image_style = $this->imageStyleStorage->load($this->getSetting('image_style'));
    /** @var \Drupal\file\FileInterface[] $images */
    foreach ($images as $delta => $image) {
      $image_uri = $image->getFileUri();
      $url = $image_style ? $image_style->buildUrl($image_uri) : \Drupal::service('file_url_generator')->generateAbsoluteString($image_uri);
      $url = \Drupal::service('file_url_generator')->transformRelative($url);

      // Add cacheability metadata from the image and image style.
      $cacheability = CacheableMetadata::createFromObject($image);
      if ($image_style) {
        $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($image_style));
      }

      // Add a timestamp to the URL to break any front-end proxy cache whenever we regenerate the bg image.
      if (strpos($url, '?')) {
        $url .= '&c=' . time();
      }
      else {
        $url .= '?c=' . time();
      }

      $elements[$delta] = [
        '#theme' => 'bg_img_formatter',
        '#url' => $url,
      ];
      $cacheability->applyTo($elements[$delta]);
    }

    return $elements;
  }

}
